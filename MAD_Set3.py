"""The pressure required to sink a large, heavy object in the soft homogeneous soil, that lies above
the hard-base soil, can be predicted by the pressure required to sink smaller objects in the same
soil. The bridge-foundations can be modeled as circular plates. The pressure p required to sink
a circular plate of radius r in the soft soil to a certain depth d can be approximated by an
expression:
p(r) = k1ek2r + k3r
where k1; k2 > 0, and k3 depend on d and the consistency of the soil but not on the radius of
the plate.
a) You have the following data: a pressure of 100 N/m2 is required to sink a plate of radius
r = 0:1 m to depth d = 1 m, whereas a plate of radius r = 0:2 m requires a pressure of 120
N/m2 and a plate of radius r = 0:3 m requires a pressure of 150 N/m2 to get sunk to the
same depth d. Formulate the system of equations to be solved for the coefficients k1; k2,
and k3 and the Jacobian matrix of the resulting system."""

import numpy as np


import autograd as ag
from autograd import grad, jacobian

p = np.transpose(np.array([100, 120, 150]))
r = np.array([0.1,0.2,0.3])
d = np.array([1, 1, 1])
#k = (k1*e^k2, k3)


A=np.zeros((3,2))
for i in range(3):

    A[i,0]=np.exp(r[i])
    A[i,1]=r[i]

"""k=np.dot(np.linalg.pinv(A),p)"""

def Function(x,r,p):
    y=np.zeros((3,1))
    for i in range(3):
        y[i]=x[0]*np.exp(x[1]*r[i])+x[2]*r[i]-p[i]
    return y


def Jacobian(x,r):
    J=np.zeros((3,3))
    for i in range(3):
        J[i, 0] = np.exp(r[i] + x[1])
        J[i, 1] = np.exp(r[i] + x[1]) * x[0] * r[i]
        J[i, 2] = r[i]
    return J

Xi=np.array([1,7,1])
#print(np.exp(r[0]) * np.exp(Xi[1]))
#print(np.linalg.matrix_rank(Jacobian(Xi,r))) JACOBIAN IS NOT FULL RANK
#print(Jacobian(Xi,r))
#J=Jacobian(Xi,r)

"""b) Implement Newton’s method to find k1; k2, and k3. Set tolerance for convergence to 10−5"""

def newton(X,r,p,function,jacobian,imax = 1e5,tol = 0.00001):
    for i in range(int(imax)):
        J = jacobian(X,r) #jacobian(X,r) # calculate jacobian J = df(X)/dY(X)
        Y = function(X,r,p) # calculate function Y = f(X)
        dX = np.dot(np.linalg.pinv(J),Y) # solve for increment from JdX = Y
        for i in range(len(X)):  # step X by dX
            X[i]=X[i]-dX[i]
        if np.linalg.norm(dX)<tol: # break if converged
            print('converged.')
            break
    return X

kn=newton(Xi,r,p,Function,Jacobian)
print(kn)
ka=np.array([kn[0]*np.exp(kn[1]),kn[2]])

#kn[1]=np.exp(kn[1])
print(np.dot(A,ka))



